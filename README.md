# GitOps using fluxctl


## Requirements 
* Kubernetes Cluster 
* GitLab Repo
* fluxctl

## Installing fluxctl
[Installing fluxctl](https://docs.fluxcd.io/en/1.18.0/references/fluxctl.html#installing-fluxctl)

### Create flux namespace on Kubernetes Cluster
```
 kubectl create namespace flux
```

### Export your GitLab User Name
```
export GLUSER=[your GitLab username]
```

### Run the following command on Kuberentes Cluster
```
fluxctl install \
--git-user=${GLUSER} \
--git-email=${GLUSER}@gmail.com \
--git-url=git@gitlab.com:${GLUSER}/gitops-demo \
--git-path=namespaces,workloads \
--namespace=flux | kubectl apply -f -
```

### Check the rollout status of deployment 
```
 kubectl -n flux rollout status deployment/flux
 ```

 ### Get the RSA Key and add it to GitLab Repo
 [GitLab - SSH Key Setup](https://www.tutorialspoint.com/gitlab/gitlab_ssh_key_setup.htm)
 ```
 fluxctl identity --k8s-fwd-ns flux
```

### Run the sync comnand to sync repo with  Kubernetes cluster via fluxctl 
```
 fluxctl sync --k8s-fwd-ns flux
 ```

 ### Confirm Deployment of Nginx Pods 
```
 kubectl get pods --all-namespaces
 ```
